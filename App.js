import React, {Component} from 'react';
import {
  AppState,
  AsyncStorage,
  BackHandler,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {WebView} from 'react-native-webview';

import {Linking, Notifications} from 'expo';

import {preInjectedJsCode} from './src/api';
import {
  isPushNotificationPermissionError,
  getExpoPushDeviceTokenError,
  storeExpoPushDeviceTokenError,
} from './src/messages';
import {PreLoadingSpinner, ViewLoadingSpinner} from './src/screens/Spinner';
import {styles} from './src/styles';
import {
  isValidToken,
  onMessageResolver,
  isPushNotificationPermitted,
  getExpoPushDeviceToken,
  setNotificationConfig,
  storeExpoPushDeviceToken,
} from './src/utils';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialUrl: 'Login',
      preLoading: true,
      viewLoading: true,
      hasPushDeviceToken: false,
      pushDeviceToken: 'NotYet',
    };

    this.webview = {
      canGoBack: false,
      reference: null,
    };
  }

  componentDidMount() {
    this.preLoad();
    this.preListen();
  }

  render() {
    let {preLoading, viewLoading} = this.state;

    return preLoading ? (
      <PreLoadingSpinner />
    ) : (
      <SafeAreaView style={styles.style_safer}>
        <KeyboardAvoidingView style={styles.style_container} behavior="padding" enabled>
          <StatusBar barStyle="light-content" />
          <WebView
            ref={_webview => {
              this.webview.reference = _webview;
            }}
            source={{uri: this.getInitialUrl()}}
            style={styles.style_webview}
            textZoom={100}
            //
            allowsInlineMediaPlayback={false}
            javaScriptEnabled={true}
            mediaPlaybackRequiresUserAction={false}
            sharedCookiesEnabled={true}
            thirdPartyCookiesEnabled={true}
            //
            allowsBackForwardNavigationGestures={true}
            onNavigationStateChange={_navigationState => {
              this.webview.canGoBack = _navigationState.canGoBack;
            }}
            //
            injectedJavaScript={preInjectedJsCode()}
            onLoad={() => this.setState({viewLoading: false})}
            onMessage={event => onMessageResolver(event.nativeEvent, this.webview.reference)}
          />
        </KeyboardAvoidingView>
        {this.state.viewLoading && <ViewLoadingSpinner />}
      </SafeAreaView>
    );
  }

  preLoad = async () => {
    await this.setPushDeviceToken();
    this.setState({
      preLoading: false,
    });
  };

  preListen = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackevent);
    Linking.addEventListener('url', this.handleUrl);
    Linking.getInitialURL()
      .then(url => {
        if (url.indexOf('tmaxcloudspace') !== -1) {
          this.setState({initialUrl: url});
        }
      })
      .catch(e => console.error(e));
    Notifications.addListener(this.handleNotification);
    setNotificationConfig();
  };

  getInitialUrl = () => {
    const deviceParam = this.state.hasPushDeviceToken ? '?DEVICEID=' + this.state.pushDeviceToken : '';

    if (this.state.initialUrl === 'Login') {
      //PRODUCTION cloudspace return 'https://www.tmaxcloudspace.com/#!/login' + deviceParam;

      //QA drone return 'https://oqa.tmaxcloudspace.com/#!/login' + deviceParam;

      //DEV larva
      return 'https://csdev.tmaxcloudspace.com/#!/login' + deviceParam;
    } else {
      return this.state.initialUrl + deviceParam;
    }
  };

  setPushDeviceToken = async () => {
    let pushDeviceToken = await AsyncStorage.getItem('PUSHDEVICETOKEN');
    if (isValidToken(pushDeviceToken)) {
      this.setState({
        hasPushDeviceToken: true,
        pushDeviceToken: pushDeviceToken,
      });
      return true;
    }

    const permitStatus = await isPushNotificationPermitted();
    if (permitStatus) {
      pushDeviceToken = await getExpoPushDeviceToken();
      if (isValidToken(pushDeviceToken)) {
        const storeStatus = await storeExpoPushDeviceToken(pushDeviceToken);
        if (storeStatus) {
          this.setState({
            hasPushDeviceToken: true,
            pushDeviceToken: pushDeviceToken,
          });
          return true;
        } else {
          alert(storeExpoPushDeviceTokenError);
        }
      } else {
        alert(getExpoPushDeviceTokenError);
      }
    } else {
      alert(isPushNotificationPermissionError);
    }
    return false;
  };

  handleBackevent = () => {
    if (this.webview.canGoBack && this.webview.reference) {
      this.webview.reference.goBack();
      return true;
    } else {
      return false;
    }
  };

  handleNotification = notification => {
    if (Platform.OS !== 'ios' && AppState.currentState === 'active' && notification.origin === 'received') {
      Notifications.dismissNotificationAsync(notification.notificationId);
    } else {
      if (notification.origin === 'selected' && Object.keys(notification.data).length !== 0) {
        this.setState({initialUrl: notification.data.onClickUrl + '?t=' + Date.now()});
      }
    }
  };

  handleUrl = object => {
    const {path} = Linking.parse(object.url);

    if (path.length !== 0) {
      this.setState({initialUrl: 'https://' + path + '?t=' + Date.now()});
    }
  };
}

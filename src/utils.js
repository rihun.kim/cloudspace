import React from 'react';
import {AsyncStorage, Platform, Share} from 'react-native';

import {Notifications} from 'expo';
import * as Contacts from 'expo-contacts';
import * as Permissions from 'expo-permissions';
import * as WebBrowser from 'expo-web-browser';

const getContacts = async () => {
  let contacts = [];

  if (await getMultiPermissions(['contacts'])) {
    const {data} = await Contacts.getContactsAsync({fields: [Contacts.PHONE_NUMBERS]});

    if (data.length > 0) {
      for (i = 0; i < data.length; ++i) {
        contacts.push([data[i].name, data[i].phoneNumbers[0].number]);
      }
    }

    return contacts;
  } else {
    return 'ERROR: contacts permission is not allowed.';
  }
};

const getPopupWebBrowser = async url => {
  if (Platform.OS === 'ios') {
    await WebBrowser.openBrowserAsync(url);
  }
};

const getMultiPermissions = async types => {
  let notGrantedTypes = [];
  const {permissions} = await Permissions.askAsync(...types);

  for (key in permissions) {
    if (permissions[key].status !== 'granted') {
      notGrantedTypes.push(key);
    }
  }

  if (notGrantedTypes.length) {
    alert('[ ' + notGrantedTypes + ' ] 에 대한 권한 허가를 해주셔야 관련 기능을 사용할 수 있습니다.');
    return false;
  } else {
    return true;
  }
};

const getSharer = async message => {
  await Share.share({message});
};

export const isValidToken = token => {
  if (token === null || token === 'NotYet' || token === '' || token.indexOf('PushToken') === -1) {
    return false;
  } else {
    return true;
  }
};

export const onMessageResolver = async (event, webview) => {
  try {
    const setting = JSON.parse(event.data);

    if (setting.service === 'tos.service.PackageManager.alertMessage') {
      alert(setting.data);
    } else if (setting.service === 'tos.service.PackageManager.checkPermission') {
      getMultiPermissions(setting.data);
    } else if (setting.service === 'tos.service.PackageManager.shareMessage') {
      getSharer(setting.data);
    } else if (setting.service === 'tos.service.PackageManager.getAppToken') {
      const appToken = await AsyncStorage.getItem('PUSHDEVICETOKEN');
      webview.injectJavaScript('Top.LocalService._execute(' + setting.serial + ',' + JSON.stringify(appToken) + ')');
    } else if (setting.service === 'tos.service.Contacts.getContacts') {
      const contacts = await getContacts();
      webview.injectJavaScript('Top.LocalService._execute(' + setting.serial + ',' + JSON.stringify(contacts) + ')');
    } else if (setting.service === 'tos.service.WebBrowser.loadUrl') {
      getPopupWebBrowser(setting.data);
    }
  } catch (e) {
    console.error(e);
  }
};

export const getExpoPushDeviceToken = async () => {
  try {
    return await Notifications.getExpoPushTokenAsync();
  } catch (e) {
    console.error(e);
    return 'Error: getting expo push device token fails.';
  }
};

export const isPushNotificationPermitted = async () => {
  try {
    const {status: existingStatus} = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;

    if (existingStatus !== 'granted') {
      const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    return finalStatus === 'granted' ? true : false;
  } catch (e) {
    console.error(e);
    return false;
  }
};

export const setNotificationConfig = async () => {
  if (Platform.OS === 'ios') {
  } else {
    await Notifications.createChannelAndroidAsync('sound', {
      name: '알림 방식',
      sound: true,
    });
  }
};

export const storeExpoPushDeviceToken = async pushDeviceToken => {
  try {
    AsyncStorage.setItem('PUSHDEVICETOKEN', pushDeviceToken);
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
};

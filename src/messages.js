export const defaultError =
  '에러가 발생하였습니다. \n앱을 종료하신 뒤, 실행해주세요.';

export const isPushNotificationPermissionError =
  '[ 에러 해결 가이드 ]\n\n최초 설치 후 실행 시에는 알림을 반드시 허용하셔야 클라우드 스페이스 관련 기능을 원활히 사용할 수 있습니다. 설정 창에서 클라우드 스페이스 알림을 허용 후 앱을 재실행 해주세요.';

export const getExpoPushDeviceTokenError =
  '[ 에러 해결 가이드 ]\n\n푸시 알림을 위한 디바이스 토큰 생성에 실패하였습니다. 앱 종료 후, 네트워크 연결 상태를 확인 후 재실행 바랍니다.';

export const storeExpoPushDeviceTokenError =
  '[ 에러 해결 가이드 ]\n\n푸시 디바이스 토큰을 저장하는 데 실패하였습니다. 앱 종료 후, 재 실행 바랍니다.';

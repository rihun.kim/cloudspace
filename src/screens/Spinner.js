import React from 'react';
import {ActivityIndicator, Text, View} from 'react-native';

import {styles} from '../styles';

export const PreLoadingSpinner = () => {
  return (
    <View style={styles.style_indicator}>
      <Text style={styles.style_indicator_text}>안녕하세요!</Text>
      <ActivityIndicator size="large" />
    </View>
  );
};

export const ViewLoadingSpinner = () => {
  return (
    <View style={styles.style_indicator}>
      <Text style={styles.style_indicator_text}>오늘도 반가워요:)</Text>
      <ActivityIndicator size="large" />
    </View>
  );
};

import React from 'react';
import {Image, SafeAreaView, StatusBar, Text, View} from 'react-native';

import {styles} from '../styles';

const Error = ({exitMessage}) => {
  return (
    <SafeAreaView style={styles.style_safer}>
      <View style={styles.style_alert}>
        <StatusBar barStyle="light-content" />
        <Image
          source={require('../../assets/error.png')}
          style={styles.style_alert_image}
        />
        <Text style={styles.style_alert_text}>{exitMessage}</Text>
      </View>
    </SafeAreaView>
  );
};

export default Error;

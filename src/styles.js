import {Platform, StatusBar, StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  style_safer: {
    backgroundColor: '#000000',
    flex: 1,
  },
  style_container: {
    flex: 1,
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
  },
  style_indicator: {
    backgroundColor: '#FFFFFF',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    opacity: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  style_indicator_text: {
    margin: 20,
  },
  style_webview: {
    flex: 1,
  },
  style_alert: {
    alignItems: 'center',
    backgroundColor: '#27B1CC',
    flex: 1,
    fontSize: 20,
    justifyContent: 'center',
    padding: 50,
  },
  style_alert_text: {
    textAlign: 'center',
  },
  style_alert_image: {
    width: '30%',
    height: '20%',
    resizeMode: 'contain',
  },
});

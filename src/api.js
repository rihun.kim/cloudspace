let isInitialized = false;

export const preInjectedJsCode = () => {
  if (isInitialized) {
    return '';
  }

  const jsCode = `
    try {
      Top.LocalService = function() {
        LocalService.prototype = Object.create(Top.prototype);
        LocalService.prototype.constructor = LocalService;
  
        function LocalService(obj) {
          Object.assign(this, obj)
        };

        LocalService._currentSerial = 0;
        LocalService._requests = {};

        LocalService.acall = function(setting) {
          let serial = ++this._currentSerial;
          let promise = new Promise((resolve, reject) => {
            this._requests[serial] = {'resolve':resolve, 'reject':reject};
          });
          setting.serial = serial;
          window.ReactNativeWebView.postMessage(JSON.stringify(setting));
          return promise;
        };
  
        LocalService._execute = function(serial, output) {
          this._requests[serial].resolve(output);
        };

        return LocalService;
      }();

      //DEBUG
      //Top.LocalService.acall({service:'tos.service.PackageManager.alertMessage', data:'얼럿'});
      //Top.LocalService.acall({service:'tos.service.PackageManager.checkPermission', data:['audioRecording', 'camera','cameraRoll', 'contacts'] });
      //Top.LocalService.acall({service:'tos.service.PackageManager.shareMessage', data:'메시지만 보내요!'});
      //Top.LocalService.acall({service:'tos.service.PackageManager.getAppToken'}).then((data) => {
      //  alert(data);
      //});
      //Top.LocalService.acall({service:'tos.service.Contacts.getContacts'}).then((data) => {
      //  alert(data.length, data[0][0], data[0][1]);
      //});
      //Top.LocalService.acall({service:'tos.service.WebBrowser.loadUrl', data:'https://www.yahoo.com/'});
    } catch(e) {
      alert(e);
    }
  `;

  isInitialized = true;

  return jsCode;
};
